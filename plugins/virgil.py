
import random,os

bot.virgil_quotes = quotes = open(os.path.join(bot.current_dir,"plugins","data","virgil.txt"),"r").read().split("\n")

@hook.cmd
def virgil(input):
    "Get a random Virgil quote."
    return(random.choice(bot.virgil_quotes))
