import requests
from datetime import datetime


API_URL = "https://coinmarketcap-nexuist.rhcloud.com/api/{}"


# aliases
@hook.cmd
def bitcoin(input):
    """,bitcoin -- Returns current bitcoin value """
    # alias
    return(hook.cmds['cryptocurrency']("btc"))


@hook.cmd
def litecoin(input):
    """,litecoin -- Returns current litecoin value """
    # alias
    return(hook.cmds['cryptocurrency']("ltc"))


@hook.cmd
def dogecoin(input):
    """,dogecoin -- Returns current dogecoin value """
    # alias
    return(hook.cmds['cryptocurrency']("doge"))


@hook.cmd
def darkcoin(input):
    """,darkcoin -- Returns current darkcoin/dash value """
    # alias
    return(hook.cmds['cryptocurrency']("dash"))
    
    
@hook.cmd
def zetacoin(input):
    """,zetacoin -- Returns current Zetacoin value """
    # alias
    return(hook.cmds['cryptocurrency']("zet"))
    

# main command
@hook.cmd
def cryptocurrency(text):
    """,cryptocurrency <ticker> -- Returns current value of a cryptocurrency """
    try:
        encoded = text
        request = requests.get(API_URL.format(encoded))
        request.raise_for_status()
    except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError) as e:
        return "Could not get value: {}".format(e)

    data = request.json()

    if "error" in data:
        return "{}.".format(data['error'])

    updated_time = datetime.fromtimestamp(float(data['timestamp']))
    if (datetime.today() - updated_time).days > 2:
        # the API retains data for old ticker names that are no longer updated
        # in these cases we just return a "not found" message
        return "Currency not found."

    change = float(data['change'])

    return "{} // {:,.2f} USD - {:,.7f} BTC // {} change".format(data['symbol'].upper(),
                                                                            float(data['price']['usd']),
                                                                            float(data['price']['btc']),
                                                                            change)

