#Plugin from http://github.com/CloudBotIRC/Cloudbot

import requests,isodate,time,re

dev_key = bot.config['API_keys']['youtube']
base_url = 'https://www.googleapis.com/youtube/v3/'
api_url = base_url + 'videos?part=contentDetails%2C+snippet%2C+statistics&id={}&key={}'
search_api_url = base_url + 'search?part=id&maxResults=1'
playlist_api_url = base_url + 'playlists?part=snippet%2CcontentDetails%2Cstatus'
video_url = "http://youtu.be/%s"
err_no_api = "The YouTube API is off in the Google Developers Console."

def pluralize(num=0, text=''):
    """
    Takes a number and a string, and pluralizes that string using the number and combines the results.
    :rtype: str
    """
    return "{:,} {}{}".format(num, text, "s"[num == 1:])

def get_text_list(list_, last_word='or'):
    """
    >> get_text_list(['a', 'b', 'c', 'd'])
    'a, b, c or d'
    >> get_text_list(['a', 'b', 'c'], 'and')
    'a, b and c'
    >> get_text_list(['a', 'b'], 'and')
    'a and b'
    >> get_text_list(['a'])
    'a'
    >> get_text_list([])
    ''
    """
    if len(list_) == 0:
        return ''
    if len(list_) == 1:
        return list_[0]
    return '%s %s %s' % (
        # Translators: This string is used as a separator between list elements
        ', '.join([i for i in list_][:-1]),
        last_word, list_[-1])

def format_time(seconds, count=3, accuracy=6, simple=False):
    """
    Takes a length of time in seconds and returns a string describing that length of time.
    This function has a number of optional arguments that can be combined:
    SIMPLE: displays the time in a simple format
    >> format_time(SECONDS)
    1 hour, 2 minutes and 34 seconds
    >> format_time(SECONDS, simple=True)
    1h 2m 34s
    COUNT: how many periods should be shown (default 3)
    >> format_time(SECONDS)
    147 years, 9 months and 8 weeks
    >> format_time(SECONDS, count=6)
    147 years, 9 months, 7 weeks, 18 hours, 12 minutes and 34 seconds
    """

    if simple:
        periods = [
            ('c', 60 * 60 * 24 * 365 * 100),
            ('de', 60 * 60 * 24 * 365 * 10),
            ('y', 60 * 60 * 24 * 365),
            ('mo', 60 * 60 * 24 * 30),
            ('d', 60 * 60 * 24),
            ('h', 60 * 60),
            ('m', 60),
            ('s', 1)
        ]
    else:
        periods = [
            (('century', 'centuries'), 60 * 60 * 24 * 365 * 100),
            (('decade', 'decades'), 60 * 60 * 24 * 365 * 10),
            (('year', 'years'), 60 * 60 * 24 * 365),
            (('month', 'months'), 60 * 60 * 24 * 30),
            (('day', 'days'), 60 * 60 * 24),
            (('hour', 'hours'), 60 * 60),
            (('minute', 'minutes'), 60),
            (('second', 'seconds'), 1)
        ]

    periods = periods[-accuracy:]

    strings = []
    i = 0
    for period_name, period_seconds in periods:
        if i < count:
            if seconds > period_seconds:
                period_value, seconds = divmod(seconds, period_seconds)
                i += 1
                if simple:
                    strings.append("{}{}".format(period_value, period_name))
                else:
                    if period_value == 1:
                        strings.append("{} {}".format(period_value, period_name[0]))
                    else:
                        strings.append("{} {}".format(period_value, period_name[1]))
        else:
            break

    if simple:
        return " ".join(strings)
    else:
        return get_text_list(strings, "and")

def get_video_description(video_id):
    
    json = requests.get(api_url.format(video_id, dev_key)).json()

    if json.get('error'):
        if json['error']['code'] == 403:
            return err_no_api
        else:
            return

    data = json['items']
    snippet = data[0]['snippet']
    statistics = data[0]['statistics']
    content_details = data[0]['contentDetails']

    out = '\x02{}\x02'.format(snippet['title'])

    if not content_details.get('duration'):
        return out

    length = isodate.parse_duration(content_details['duration'])
    out += ' - length \x02{}\x02'.format(format_time(int(length.total_seconds()), simple=True))
    try:
        total_votes = float(statistics['likeCount']) + float(statistics['dislikeCount'])
    except:
        total_votes = 0
    if total_votes != 0:
        # format
        likes = pluralize(int(statistics['likeCount']), "like")
        dislikes = pluralize(int(statistics['dislikeCount']), "dislike")

        percent = 100 * float(statistics['likeCount']) / total_votes
        out += ' - {}, {} (\x02{:.1f}\x02%)'.format(likes,
                                                    dislikes, percent)

    if 'viewCount' in statistics:
        views = int(statistics['viewCount'])
        out += ' - \x02{:,}\x02 view{}'.format(views, "s"[views == 1:])

    uploader = snippet['channelTitle']

    upload_time = time.strptime(snippet['publishedAt'], "%Y-%m-%dT%H:%M:%S.000Z")
    out += ' - \x02{}\x02 on \x02{}\x02'.format(uploader,
                                                time.strftime("%Y.%m.%d", upload_time))

    if 'contentRating' in content_details:
        out += ' - \x034NSFW\x02'

    return out

@hook.regex(*(r'(?:youtube.*?(?:v=|/v/)|youtu\.be/|yooouuutuuube.*?id=)([-_a-z0-9]+)', re.I))
def yt_test(match):
    return(get_video_description(match.group(1)))

@hook.cmd("youtube", "you", "yt", "y")
def youtube(text):
    """youtube <query> -- Returns the first YouTube search result for <query>."""
    if not dev_key:
        return "This command requires a Google Developers Console API key."
    if not text=="":
        json = requests.get(search_api_url, params={"q": text, "key": dev_key, "type": "video"}).json()

        if json.get('error'):
            if json['error']['code'] == 403:
                return err_no_api
            else:
                return 'Error performing search.'

        if json['pageInfo']['totalResults'] == 0:
            return 'No results found.'

        video_id = json['items'][0]['id']['videoId']

        return get_video_description(video_id) + " - " + video_url % video_id

