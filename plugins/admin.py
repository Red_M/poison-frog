
import time
import os
import sys
import traceback


@hook.cmd
def set_game(input,message=None):
    bot.set_game(input)

@hook.cmd
def py_exec(input,message=None):
    if bot.permission_check(message.author,bot.get_highest_group()):
        if input.startswith('```'):
            input = input[3:]
        if input.startswith('python'): # dumb smart code clean
            input = input[6:]
        if input.endswith('```'):
            input = input[:-3]
        exec(input)

@hook.cmd
def voice_control(input,message=None):
    if bot.permission_check(message.author,bot.get_highest_group()):
        input_split = input.split(" ")
        modes = [
            "disconnect",
            "join"
            ]
        if input_split[0] in modes:
            if input_split[0]=="disconnect":
                try:
                    client.voice.disconnect()
                    bot.v_player = None
                    return("Done.")
                except Exception as e:
                    type_, value_, traceback_ = sys.exc_info()
                    ex = traceback.format_exception(type_, value_, traceback_)
                    trace = ""
                    for data in ex:
                        trace = str(trace+data)
                    return('```'+trace+'```')
            if input_split[0]=="join":
                channel = " ".join(input_split[1:-1])
                video = 'https://www.youtube.com/watch?v='+input_split[-1]
                resolved = bot.resolve_voice_channel(channel,message.channel.server.id)
                if not resolved==None:
                    try:
                        client.join_voice_channel(resolved)
                        bot.v_player = client.voice.create_ytdl_player(video,use_avconv=True,after=client.voice.disconnect)
                        bot.v_player.start()
                        return("Done.")
                    except Exception as e:
                        client.voice.disconnect()
                        type_, value_, traceback_ = sys.exc_info()
                        ex = traceback.format_exception(type_, value_, traceback_)
                        trace = ""
                        for data in ex:
                            trace = str(trace+data)
                        
                        return('```'+trace+'```')
                else:
                    return("Channel '"+channel+"' is not a voice channel in this server.")
    else:
        return("Sorry, you're not high enough in my permissions to do that.")

@hook.cmd
def img_test(input,message=None):
    if bot.permission_check(message.author,bot.get_highest_group()):
        file = open(os.path.join(bot.current_dir,"plugins","data","load.gif"), "rb")
        filename = "test.gif"
        #client.send_raw_file(message.channel, filename, file)
    return("",True)

@hook.cmd
def resolve(input,message=None):
    if bot.permission_check(message.author,bot.get_highest_group()):
        return(bot.mentions(input))

@hook.cmd
def list_sieves(input,message=None):
    if bot.permission_check(message.author,bot.get_highest_group()):
        return(hook.sieves)

@hook.cmd
def rtl_char(input,message=None):
    "Right to left control character."
    if bot.permission_check(message.author,bot.get_highest_group()):
        return(u"->\u202E--")

@hook.cmd
def ban(input,message=None):
    "ban <user> -- Bans <user> from server."
    if bot.permission_check(message.author,bot.get_highest_group()):
        if len(message.mentions)>0 and input.startswith("<@"):
            client.ban(message.channel.server,message.mentions[0])
            return("Done.")

@hook.cmd
def kick(input,message=None):
    "kick <user> -- Kick <user> from server if able.\nAdmin only."
    if len(message.mentions)>0 and input.startswith("<@"):
        client.kick(message.channel.server,message.mentions[0])
        return("Done.")

@hook.cmd
def get_invite(input,message=None):
    "get_invite <server_id> -- Gets an invite for <server_id> if able."
    if bot.permission_check(message.author,bot.get_highest_group()):
        try:
            return(client.create_invite(message.channel.server).url)
        except:
            return("Can't get an invite.")

@hook.cmd
def nick(input,message=None):
    "nick <input> -- Change my username to <input>"
    if bot.permission_check(message.author,bot.get_highest_group()):
        bot.change_username(input)
        return("Done.")

@hook.cmd
def say(input,message=None):
    "say [target] <input> -- says <input> or says <input> to channel/user.\nAdmin only."
    if bot.permission_check(message.author,bot.get_highest_group()):
        if len(message.mentions)>0 and input.startswith("<@"):
            client.send_message(message.mentions[0], " ".join(input.split(" ")[1:]))
            return('',True)
        if len(input.split(" "))>1 and input.startswith("<#"):
            channel = bot.resolve_channel_id("".join(input.split("<#")[1:]).split("> ")[0])
            if not channel==None:
                try:
                    client.send_message(channel, " ".join(input.split(" ")[1:]))
                except Exception as e:
                    return(channel)
            else:
                return("Unable to find that channel")
            return('',True)
        return(input,True)
    else:
        return("Nope.avi")

@hook.cmd
def leave(input,message=None,msg=None):
    "leave -- Leave the current server.\nAdmin only."
    client.delete_message(msg)
    if bot.permission_check(message.author,bot.get_highest_group()):
        client.send_message(message.channel, "Goodbye.")
        client.leave_server(message.channel.server)
    else:
        return("Nope.avi")

@hook.cmd("perm_check", "permission_check")
def permission_check(input,message=None):
    "permission_check <group> -- Checks if you are in my permissions under <group>"
    try:
        result = bot.permission_check(message.author,input)
        if result==True:
            return(message.author.name+" is in the group: "+input)
        else:
            return(message.author.name+" is not in the group: "+input)
    except Exception as e:
        return("No such group.")

@hook.cmd("perms", "permissions")
def permissions(input,message=None):
    ",permissions -- Coming Soon."
    return("Soon(tm)")

@hook.cmd
def servers(input,message=None):
    "servers <list|invite|del> [server_id] -- Admin interface for server control."
    if bot.permission_check(message.author,bot.get_highest_group()):
        split = input.split(" ")
        cmds = [
            "list",
            "invite",
            "del"
        ]
        if len(split)>0:
            if split[0] in cmds:
                if split[0]=="list":
                    servers = []
                    for data in client.servers:
                        servers.append("("+data.id+") "+data.name)
                    client.send_message(message.author,", ".join(servers))
                    return
                if split[0]=="del":
                    for data in client.servers:
                        for server in split[1:]:
                            if data.id==server:
                                if data.name in bot.config["servers"]:
                                    del bot.config["servers"][data.name]
                                    bot.save_config()
                                client.leave_server(data)
                                client.send_message(message.author,"Done.")
                                return
                if split[0]=="invite":
                    for server in client.servers:
                        if server.id==split[1]:
                            return(client.create_invite(server).url)
    users = 0
    channels = 0
    for server in client.servers:
        users += len(server.members)
        channels += len(server.channels)
    users = str(users)
    channels = str(channels)
    return("I am in "+str(len(client.servers))+" server(s).\nI am in "+channels+" text channel(s).\nI can see "+users+" users.")

@hook.cmd
def message_control(input,message=None):
    "message_control [channel] <logs|del/remove> <amount of messages> -- Controls messages in [#channel] with <logs|del/remove>\nAssumes the current channel if no [#channel] is given."
    split = input.split(" ")
    cmds = [
        "logs",
        "log",
        "del",
        "remove"
    ]
    length = 100
    if len(split)==3:
        channel = split[0]
        cmd = split[1]
        try:
            length = int(split[2])
        except:
            pass
    elif len(split)==2 or len(split)==1:
        channel = message.channel
        cmd = split[0]
        if len(split)==2:
            try:
                length = int(split[1])
            except:
                pass
    if cmd in cmds:
        if cmd=="logs" or cmd=="log":
            logs = client.logs_from(channel,length)
            reversed_logs = []
            for data in logs:
                reversed_logs.append(data)
            log_pages = ""
            for log_message in list(reversed(reversed_logs)):
                log_pages = log_pages+str("```"+log_message.author.name+": "+log_message.content.replace("```",""))[:1993]+"```\n"
            for page in bot.pagenation(log_pages):
                client.send_message(message.author,page)
            return
        if bot.permission_check(message.author,bot.get_highest_group()):
            if cmd=="del" or cmd=="remove":
                logs = client.logs_from(channel,length)
                for data in logs:
                    try:
                        client.delete_message(data)
                    except:
                        pass
                return
    

@hook.cmd
def ignore(input,message=None):
    ",ignore <add|del|list> [user] -- Adds/deletes/lists the ignored user(s).\nAdd/delete is Admin only."
    split = input.split(" ")
    cmds = [
        "list",
        "add",
        "del",
        "remove"
    ]
    if len(split)>0:
        if split[0] in cmds:
            if split[0]=="list":
                response = ""
                for data in bot.config['ignored']:
                    if data=="channels":
                        response = response+data+": "
                        for ignored_channel in bot.config['ignored'][data]:
                            end_channel = bot.resolve_channel_id(ignored_channel)
                            if not end_channel==None:
                                if len(end_channel.name)>1:
                                    channelname = end_channel.name[0]+"\x00"+end_channel.name[1:]
                                else:
                                    channelname = end_channel.name[0]+"\x00"
                                if not channelname in response:
                                    response = response+"#"+channelname+"("+ignored_channel+"), "
                            else:
                                response = response+ignored_channel+", "
                        response = response[:-2]+"\n\n"
                    if data=="user_ids":
                        response = response+data+": "
                        for ignored_user in bot.config['ignored'][data]:
                            end_user = bot.resolve_user_id(ignored_user)
                            if not end_user==None:
                                if len(end_user.name)>1:
                                    username = end_user.name[0]+"\x00"+end_user.name[1:]
                                else:
                                    username = end_user.name[0]+"\x00"
                                if not username in response:
                                    response = response+username+"("+ignored_user+"), "
                            else:
                                response = response+ignored_user+", "
                        response = response[:-2]+"\n\n"
                                
                    if data=="usernames":
                        response = response+data+": "+", ".join(bot.config['ignored'][data])+"\n\n"
                if len(response)>2000:
                    pages = bot.pagenation(response,1993)
                    for page in pages:
                        client.send_message(message.channel, '```'+page+'```')
                return
            if bot.permission_check(message.author,bot.get_highest_group()):
                if split[0]=="add":
                    reply = "Added:"
                    for user in split[1:]:
                        if (user.startswith("<@") or user.startswith("<#")) and user.endswith(">"):
                            user_data = user[2:-1]
                            if user.startswith("<#"):
                                if not user_data in bot.config['ignored']['channels']:
                                    bot.config['ignored']['channels'].append(user_data)
                            if user.startswith("<@"):
                                if not user_data in bot.config['ignored']['user_ids']:
                                    bot.config['ignored']['user_ids'].append(user_data)
                        else:
                            if not user in bot.config['ignored']['usernames'] and (not user==""):
                                bot.config['ignored']['usernames'].append(user)
                        reply = reply+" "+user+", "
                    bot.save_config()
                    return(reply[:-2])
                if split[0]=="del" or split[0]=="remove":
                    reply = "Removed:"
                    for user in split[1:]:
                        if (user.startswith("<@") or user.startswith("<#")) and user.endswith(">"):
                            user_data = user[2:-1]
                            if user.startswith("<#"):
                                if user_data in bot.config['ignored']['channels']:
                                    bot.config['ignored']['channels'].remove(user_data)
                            if user.startswith("<@"):
                                if user_data in bot.config['ignored']['user_ids']:
                                    bot.config['ignored']['user_ids'].remove(user_data)
                        else:
                            if user in bot.config['ignored']['usernames'] and (not user==""):
                                bot.config['ignored']['usernames'].remove(user)
                        reply = reply+" "+user+", "
                    bot.save_config()
                    return(reply[:-2])
        else:
            return("No mode selected.\nTry "+bot.config['prefix']+"help ignore")

@hook.cmd
def ignore_event(input,message=None):
    ",ignore_event <add|del|list> [channel] -- Adds/deletes/lists the ignored channel(s) for events.\nAdd/delete is Admin only."
    split = input.split(" ")
    cmds = [
        "list",
        "add",
        "del",
        "remove"
    ]
    if len(split)>0:
        if split[0] in cmds:
            if split[0]=="list":
                response = ""
                for data in bot.config['event-ignore-channels']:
                        response = response+data+": "
                        for ignored_channel in bot.config['event-ignore-channels']:
                            end_channel = bot.resolve_channel_id(ignored_channel)
                            if not end_channel==None:
                                if len(end_channel.name)>1:
                                    channelname = end_channel.name[0]+"\x00"+end_channel.name[1:]
                                else:
                                    channelname = end_channel.name[0]+"\x00"
                                if not channelname in response:
                                    response = response+"#"+channelname+"("+ignored_channel+"), "
                            else:
                                response = response+ignored_channel+", "
                        response = response[:-2]+"\n\n"
                if len(response)>2000:
                    pages = bot.pagenation(response,1993)
                    for page in pages:
                        client.send_message(message.channel, '```'+page+'```')
                return
            if bot.permission_check(message.author,bot.get_highest_group()):
                if split[0]=="add":
                    reply = "Added:"
                    for user in split[1:]:
                        if user.startswith("<#") and user.endswith(">"):
                            user_data = user[2:-1]
                            if user.startswith("<#"):
                                if not user_data in bot.config['event-ignore-channels']:
                                    bot.config['event-ignore-channels'].append(user_data)
                        reply = reply+" "+user+", "
                    bot.save_config()
                    return(reply[:-2])
                if split[0]=="del" or split[0]=="remove":
                    reply = "Removed:"
                    for user in split[1:]:
                        if user.startswith("<#") and user.endswith(">"):
                            user_data = user[2:-1]
                            if user_data in bot.config['event-ignore-channels']:
                                bot.config['event-ignore-channels'].remove(user_data)
                        reply = reply+" "+user+", "
                    bot.save_config()
                    return(reply[:-2])
        else:
            return("No mode selected.\nTry "+bot.config['prefix']+"help ignore_event")

