
import random,os

bot.virgil_quotes = quotes = open(os.path.join(bot.current_dir,"plugins","data","vergil.txt"),"r").read().split("\n")

@hook.cmd
def vergil(input):
    "Get a random Vergil quote."
    return(random.choice(bot.virgil_quotes))
