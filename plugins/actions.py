
import random

@hook.event("!kick")
def lol_kick(input,message=None):
    return("I can't do that Dave")

def weighted_choice(choices):
   total = sum(w for c, w in choices)
   r = random.uniform(0, total)
   upto = 0
   for c, w in choices:
      if upto + w >= r:
         return c
      upto += w
   assert False, "Shouldn't get here"

@hook.event("_")
def action(input,message=None):
    if input.startswith("_") and input.endswith("_"):
        input = input[1:-1]
        split = input.split(" ")
        split_lower = []
        response = [
            ("_Croaks_",49),
            ("_Ribbits_",49),
            ("_Explodes_",1)
        ]
        for data in split:
            split_lower.append(data.lower())
        if client.user.name.lower() in split_lower or bot.mention(client.user) in split:
            return(weighted_choice(response))

@hook.event(client.user.name,client.user.name.lower(),bot.mention(client.user))
def return_hi(input,message=None):
    if input.lower().startswith("hi "):
        return("Hi <@"+message.author.id+">")