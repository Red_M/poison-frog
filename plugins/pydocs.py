import inspect
import pydoc

@hook.cmd
def py_doc(input):
    ",py_doc <input> -- Get python docs of <input>"
    func = pydoc.locate(input,forceload=1)
    response = str(pydoc.plain(pydoc.render_doc(func,forceload=1)))[:1986]
    if not ("class NoneType(object)" in response and "Python Library Documentation: NoneType object" in response):
        response = '```python\n'+response+'```'
        return(response)