#!/usr/bin/env python2
import discord
import logging
import threading
import inspect
import traceback,os
import re
import requests,isodate,time,json
import string
import sys
import math
if sys.version_info < (3, 0):
    reload(sys)
    sys.setdefaultencoding('UTF8')
sys.path += ['plugins']
sys.path += ['core']
#thread.stack_size(1024 * 512)

# Set up the logging module to output diagnostic to the console.
logging.basicConfig()

os.chdir('.' or sys.path[0])
current_dir = os.path.join(os.getcwd(),os.sep.join(sys.argv[0].split(os.sep)[0:-1]))
if current_dir.endswith("."):
    current_dir = current_dir[0:-1]
if sys.argv[0].split(os.sep)[-1] in os.listdir(current_dir):
    print("INFO: Found path")
else:
    print("INFO: Bad path")
    exit()


class Input(dict):
    def __init__(self,inp,message,msg,match):
        dict.__init__(self,inp=inp,message=message,msg=msg,match=match)

    def __getattr__(self, key):
        return self[key]

    def __setattr__(self, key, value):
        self[key] = value


class Hooks(object):
    def __init__(self):
        self.cmds = {}
        self.events = {}
        self.regexs = {}
        self.sieves = {}
        self.command = self.cmd # Alias just for those devs from Cloudbot/Skybot
    
    def _add_event(self,name,func):
        if not func==None:
            if not hasattr(func, '_filename'):
                func._filename = func.func_code.co_filename
        self.events[name] = func
    
    def cmd(self,*arg,**kwargs): # Add a command to the bot, these plugins run after the sieve plugins but BEFORE the event plugins.
        args = {}
        def command_wrapper(func):
            if isinstance(func,type(("",""))):
                func = func[0]
            if not func==None:
                if not hasattr(func, '_filename'):
                    func._filename = func.func_code.co_filename
            if 'name' in args:
                if isinstance(args['name'],type("")):
                    self.cmds[args['name']] = func
                else:
                    for data in args['name']:
                        if isinstance(data,type(("",""))):
                            for tupdata in data:
                                self.cmds[tupdata] = func
                        else:
                            self.cmds[data] = func
            else:
                self.cmds[func.func_name] = func
        if len(arg) == 1 and callable(arg[0]):
            return(command_wrapper(arg))
        else:
            if arg is not None:
                args['name'] = arg
            return(command_wrapper)
    
    def sieve(self,func):
        if func.func_code.co_argcount != 2:
            raise ValueError(
                    'sieves must take 2 arguments: (bot, input)')
        self.sieves[func.func_name] = func
        return func
    
    def event(self,*arg,**kwargs): #These plugins are simple text matches with if "text" in message, they run after command plugins
        args = kwargs
        def event_wrapper(func):
            if 'events' in args:
                for data in args["events"]:
                    self._add_event(data,func)
        if inspect.isfunction(arg):
            event_wrapper(arg)
        else:
            if arg is not None:
                args['events'] = arg
            return(event_wrapper)
    
    def regex(self,regex, flags=0, **kwargs):
        args = kwargs
        def regex_wrapper(func):
            args['name'] = func.func_name
            args['regex'] = regex
            args['re'] = re.compile(regex, flags)
            if not func==None:
                if not hasattr(func, '_filename'):
                    func._filename = func.func_code.co_filename
            self.regexs[args['re']] = func
        if inspect.isfunction(regex):
            raise ValueError("regex decorators require a regex to match against")
        else:
            return regex_wrapper
    
    def get_func_args(self,func): # Generate func._args
        argspec = inspect.getargspec(func)
        n_args = len(argspec.args)
        if argspec.defaults:
            n_args -= len(argspec.defaults)
        if argspec.keywords:
            n_args -= 1
        if argspec.varargs:
            n_args -= 1
        if n_args != 1:
            err = 'must take 1 non-keyword argument (%s)' % (
                        func.__name__)
            raise ValueError(err)

        args = []
        if argspec.defaults:
            end = bool(argspec.keywords) + bool(argspec.varargs)
            args.extend(argspec.args[-len(argspec.defaults):
                        end if end else None])
        if argspec.keywords:
            args.append(0)  # means kwargs present
        return(args)
    
    def do_sieve(self, sieve, bot, input):
        try:
            return sieve(bot, input)
        except Exception:
            print 'sieve error',
            traceback.print_exc()
            return None
    
    def dispatch(self,func,input,msg,type_func):
        args = None
        if type_func=="cmd":
            func = self.cmds[func]
        if type_func=="event":
            func = self.events[func]
        if type_func=="regex":
            func = self.regexs[func]
        if not func==None:
            if not hasattr(func, '_args'):
               func._args = self.get_func_args(func)
            args = func._args
        
        #if func._thread:
        #    bot.threads[func].put(input)
        #else:
        
        #thread.start_new_thread(self.typing_thread, (input.message.channel))
        #thread.start_new_thread(self.run, (func,input,input.message,msg,type_func))
        plugin_thread = threading.Thread(None,self.run,None,(func,input,input.message,msg,type_func))
        plugin_thread.start()
        if not type_func=="event": # events don't need to send a typing status
            typing_thread = threading.Thread(None,self.typing_thread,None,(input.message.channel,plugin_thread))
            typing_thread.start()
    
    def typing_thread(self,channel,plugin_thread):
        while plugin_thread.is_alive()==True:
            client.send_typing(channel)
            time.sleep(5)
        #client.delete_message(client.send_message(channel, ":frog:"))
    
    def run(self,func,input,message,msg,type_func):
        time.sleep(0.05)
        bot.clean_message = True # Reset the message cleaner to default
        if type_func=="event" or type_func=="regex": # force our previous msg to be None
            msg = None
        else: # We are in a requested command, we need to send that the bot is typing.
            client.send_typing(input.message.channel)
        if not func==None:
            if type_func=="regex":
                input.inp = input.match
            if not hasattr(func, '_filename'):
                func._filename = func.func_code.co_filename
            args = func._args
            if args:
                if 0 in args:
                    out = func(input.inp, **input)
                else:
                    kw = dict((key, input[key]) for key in args if key in input)
                    out = func(input.inp, **kw)
            else:
                out = func(input.inp)
            delete_msg = False
            mentions_users = True
            if out is not None:
                if isinstance(out,type(("",""))):
                    if len(out)==2:
                        (response,delete_msg) = out
                    elif len(out)==3:
                        (response,delete_msg,mentions_users) = out
                else:
                    response = unicode(out)
                response = response[:2000]
                if (not response==None) and (isinstance(response,type(u"")) or isinstance(response,type(""))):
                    for censored_string in bot.config['censored_strings']:
                        response = response.replace(censored_string,"")
                    if bot.clean_message==True:
                        response = response.replace(u"\u202E","") # Just fuck rtl overrides.
                    if delete_msg==False and msg!=None:
                        client.edit_message(msg, response, mentions=mentions_users)
                    else:
                        if not msg==None:
                            client.delete_message(msg)
                        if not response=='':
                            client.send_message(message.channel, response, mentions=mentions_users)
                else:
                    if not msg==None:
                        try:
                            client.delete_message(msg)
                        except:
                            pass
            else:
                if not msg==None:
                    try:
                        client.delete_message(msg)
                    except:
                        pass
        else:
            if not msg==None:
                try:
                    client.delete_message(msg)
                except:
                    pass
        try:
            if delete_msg==False:
                if type_func=="cmd":
                    client.delete_message(client.send_message(message.channel, ""))
        except:
            pass
        return()
                
class Bot():
    def __init__(self):
        self.__done_setup__ = False
        self.voice = None
        self.v_player = None
        self.config = {}
        self._reloaders = {}
        self._reloaders["plugins"] = time.time()
        self.commands = {}
        self.clean_message = True # cleans outputed messages from certain characters that break readability
        self.rate_limit = {}
        self.rate_limit_amount = 3.0
        self.mentions_regex = re.compile(r'<(\@|\#|\@\!|\#\!)(\d+)>')
    
    def admin_check(self,id,name):
        if name in self.config["groups"][self.get_highest_group()] or id in self.config["groups"][self.get_highest_group()]:
            return(True)
        else:
            return(False)
    
    def ignore_check(self,id,name,chan_id=None,lower=False,mode="cmd",ignored=False):
        if mode=="event":
            if chan_id in self.config['event-ignore-channels']:
                ignored = True
        for data_type in self.config['ignored']:
            if data_type=="channels":
                if not chan_id==None:
                    for data in self.config['ignored'][data_type]:
                        if str(chan_id)==data:
                            ignored = True
            if data_type=="user_ids":
                for data in self.config['ignored'][data_type]:
                    if str(id)==data:
                        ignored = True
            if data_type=="usernames":
                for data in self.config['ignored'][data_type]:
                    if lower==True:
                        data = data.lower()
                    if data.startswith("*"):
                        if name.endswith(data[1:]):
                            ignored = True
                    if data.endswith("*"):
                        if name.startswith(data[:-1]):
                            ignored = True
                    if data.startswith("*") and data.endswith("*"):
                        if data[1:-1] in name:
                            ignored = True
        if lower==True:
            name = name.lower()
            ignored = self.ignore_check(id,name,chan_id,False,mode,ignored)
        if self.config["bot_accounts_ignored"]==True:
            user = self.resolve_user_id(id)
            if hasattr(user, 'extra'):
                if "bot" in user.extra:
                    if user.extra["bot"]==True:
                        ignored = True
        if self.config["admin_overrides_ignores"]==True:
            if name in self.config["groups"][self.get_highest_group()] or id in self.config["groups"][self.get_highest_group()]:
                ignored = False
        return(ignored)
    
    def _console_log_print(self,message,console_message):
        if self.ignore_check(message.author.id,message.author.name,message.channel.id,True)==False and self.config['no_ignores_print_to_console']==True:
            if not (message.content==":frog:" and message.author.id==client.user.id): # This stops printing the empty message we send when we are clearing the typing status from discord. They're instantly deleted anyway.
                if not isinstance(message.channel,discord.channel.PrivateChannel):
                    console_message = console_message+"<"+message.channel.server.name+">[<#"+message.channel.id+">#"+message.channel.name+"] "
                else:
                    console_message = console_message+"<PM> "
                console_message = console_message+"<@"+str(message.author.id)+">"+message.author.name+":: "+message.content
                self.debugger(3,filter(lambda x: x in string.printable, console_message))
    
    def resolve_server(self,name):
        name = str(name)
        for server in client.servers:
            if server.name==name:
                return(server)
        return(None)
    
    def resolve_username(self,name):
        name = str(name)
        for server in client.servers:
            for end_user in server.members:
                if end_user.name==name:
                    return(end_user)
        return(None)
    
    def resolve_user_id(self,id):
        id = str(id)
        for server in client.servers:
            for end_user in server.members:
                if end_user.id==id:
                    return(end_user)
        return(None)
    
    def resolve_channel_id(self,id):
        for server in client.servers:
            for end_channel in server.channels:
                if end_channel.id==id:
                    return(end_channel)
        return(None)
    
    def resolve_voice_channel(self,id,server_id=None):
        for server in client.servers:
            if server_id==None or server.id==server_id:
                for end_channel in server.channels:
                    if end_channel.name==id and end_channel.type=="voice":
                        return(end_channel)
        return(None)
    
    def get_bot_member(self,message):
        if not isinstance(message.channel,discord.channel.PrivateChannel):
            bot_server_member = None
            for member in message.channel.server.members:
                if str(type(member))=="<class 'discord.member.Member'>":
                    if member.id==client.user.id:
                        bot_server_member = member
                        return(bot_server_member)
        return(None)
    
    def mentions(self,message,channels=True,users=True): # Resolve channels and users to classes for us to use.
        mess_split = re.findall(self.mentions_regex,message)
        mentions = []
        for data in mess_split:
            if not data==('',''):
                try:
                    (mention_type,id) = data
                    if mention_type.startswith('@') and users==True:
                        data = self.resolve_user_id(id)
                    elif mention_type.startswith('#') and channels==True:
                        data = self.resolve_channel_id(id)
                    if not (data in mentions and data==None):
                        mentions.append(data)
                except:
                    pass
        return(mentions)
        
    
    def get_group_from_number(self,number):
        return(self.config['group_order'][number])
    
    def get_highest_group(self):
        return(self.get_group_from_number(0))
    
    def permission_check(self,user,group):
        group = self.config["groups"][group]
        id = str(user.id)
        return(id in group)
    
    def mention(self,user):
        return(u"<@"+user.id+">"+u"\u202D") #add right to left override just incase.
    
    def trace_back(self):
        type_, value_, traceback_ = sys.exc_info()
        ex = traceback.format_exception(type_, value_, traceback_)
        trace = ""
        for data in ex:
            trace = str(trace+data)
        return(trace)
    
    def debugger(self,lvl=5,message=""):
        message = str(unicode(message))
        if lvl==0:
            lvl = "FATAL"
        if lvl==1:
            lvl = "CRITICAL"
        if lvl==2:
            lvl = "ERROR"
        if lvl==3:
            lvl = "INFO"
        if lvl==4:
            lvl = "MESSAGE"
        if lvl==5:
            lvl = "DEBUG"
        if "\n" in message:
            message = message.replace("\n","\\n")
        print(str(lvl)+": "+message)
    
    def pagenation(self, message, length=2000):
        pages = []
        page_numbers = int(math.ceil(float(len(message))/float(length)))
        for page in xrange(1,page_numbers+1):
            pages.append(message[((page-1)*length):(page*length)])
        return(pages)
    
    def super_message(self, channel, message):
        messages = self.pagenation(str(message))
        for mini_message in messages:
            client.send_message(channel,mini_message)
            time.sleep(.2) # static rate limit
    
    def reloads(self):
        if self._reloaders["plugins"]+1<time.time():
            self._reloaders["plugins"] = time.time()
            if self.__done_setup__==True:
                global reload
                reload(False)
    
    def set_game(self,game):
        client.change_presence(game=discord.Game(name=game))
    
    def save_config(self):
        json.dump(self.config, open(os.path.join(current_dir,'config'), 'w'), sort_keys=True, indent=1)
    
    def change_username(self,name):
        client.edit_profile(self.config["password"],username=name)
    
    def run_event(self,message):
        if not self.ignore_check(message.author.id,message.author.name,message.channel.id,True,"event")==True:
            for data in hook.events:
                if data in message.content:
                    inp = message.content
                    msg = None
                    input = Input(inp,message,msg,None)
                    try:
                        hook.dispatch(data,input,None,"event")
                    except Exception,e:
                        print(self.trace_back())
            for data in hook.regexs:
                inp = message.content
                m = data.search(inp)
                if m:
                    msg = None
                    input = Input(inp,message,msg,m)
                    try:
                        hook.dispatch(data,input,None,"regex")
                    except Exception,e:
                        print(self.trace_back())
    
    def run_plugin(self,message):
        response = None
        message.mentions = self.mentions(message.content,False) # override discord.py's mentions system for better mentions
        bot_server_member = self.get_bot_member(message)
        can_send_message = True #Default to true if we error on getting permissions
        try:
            if (not bot_server_member==None) and message.channel.is_private==False:
                can_send_message = message.channel.permissions_for(bot_server_member).can_send_messages
        except:
            pass
        if not self.admin_check(message.author.id,message.author.name):
            if message.author.id in self.rate_limit:
                if self.rate_limit[message.author.id]>=time.time():
                    self.rate_limit[message.author.id] = self.rate_limit[message.author.id]+self.rate_limit_amount
                    return()
                else:
                    del self.rate_limit[message.author.id]
            else:
                bot.rate_limit[message.author.id] = time.time()+bot.rate_limit_amount
        remove_rate_limit = []
        for user_id in bot.rate_limit:
            if self.rate_limit[user_id]<time.time():
                remove_rate_limit.append(user_id)
        for user_id in remove_rate_limit:
            del self.rate_limit[user_id]
        if (not (message.author.id==client.user.id or self.ignore_check(message.author.id,message.author.name,message.channel.id,True)==True)) and (can_send_message==True):
            for sieve in hook.sieves:
                sieve_return = hook.do_sieve(hook.sieves[sieve], self, Input(message.content,message,None,None))
                if sieve_return == None:
                    return
            if message.content.startswith(self.config["prefix"]) or client.user in message.mentions or message.channel.is_private==True:
                split_up = message.content.split(" ")
                if not message.content.startswith(self.config["prefix"]):
                    if client.user in message.mentions:
                        split_up = "".join(message.content.split("<@"+str(client.user.id)+"> ")[1:]).split(" ")
                        if len(split_up)>1:
                            cmd = split_up[0]
                            inp = split_up[1:]
                        else:
                            self.run_event(message)
                            return
                    if message.channel.is_private==True:
                        cmd = split_up[0]  #Channel is a private channel and therefore requires no prefix
                        inp = split_up[1:]
                else:
                    cmd = split_up[0][len(self.config["prefix"]):]
                    inp = split_up[1:]
                if cmd in hook.cmds:
                    msg = client.send_message(message.channel, "Please wait")
                    if not msg==None:
                        input = Input(" ".join(inp),message,msg,None)
                        try:
                            hook.dispatch(cmd,input,msg,"cmd")
                        except Exception,e:
                            print(self.trace_back())
                            client.delete_message(msg)
            if response==None:
                self.run_event(message)

def config():
    # reload config from file if file has changed
    config_mtime = os.stat('config').st_mtime
    if bot._config_mtime != config_mtime:
        try:
            bot.config = json.load(open('config'))
            bot._config_mtime = config_mtime
        except ValueError, e:
            print 'ERROR: malformed config!', e



bot = Bot()
hook = Hooks()

bot.current_dir = current_dir
bot._config_mtime = 0
client = discord.Client()
eval(compile(open(os.path.join('core', 'reload.py'), 'U').read(),
    os.path.join('core', 'reload.py'), 'exec'))

config()
if bot.config["bot_token"]=="":
    client.login(bot.config["username"], bot.config["password"])
else:
    client.login(bot.config["bot_token"])
bot.client = client
if not hasattr(bot, 'config'):
    exit()

if not client.is_logged_in:
    print('Logging in to Discord failed')
    exit(1)

@client.event
def on_message_edit(before, after):
    if not before.content==after.content:
        bot.reloads()
        bot.run_plugin(after)
        console_message = "EDIT: "
        bot._console_log_print(after,console_message)

@client.event
def on_message(message):
    bot.reloads()
    bot.run_plugin(message)
    console_message = ""
    bot._console_log_print(message,console_message)

        

@client.event
def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    
    reload(init=True)
    
    if bot.config['force_join_on_start_up']==True:
        for server in bot.config['servers']:
            try:
                client.accept_invite(bot.config['servers'][server])
            except:
                bot.debugger(2,"Failed to auto join: "+server)
            #try: # discord.py develop branch raises an exception if we accept an invite to a server we are already in.
            #    for data in client.servers:
            #        bot.config['servers'][data.id+"<"+data.name+">"] = client.create_invite(data).url
            #    bot.save_config()
    #        except:
    #            pass
    if not bot.config['game_text']=='':
        bot.set_game(bot.config['game_text'])
    bot.__done_setup__ = True

try:
    client.run()
except Exception as e:
    client.logout()

